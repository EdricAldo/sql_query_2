create database xyz

use xyz

create table Freeusers(
	FreeUsersId int identity(1,1) PRIMARY KEY
)

create table Premiumusers(
	PremiumUsersId int identity(1,1) PRIMARY KEY
)

create table Users(
	UsersId int identity(1,1) PRIMARY KEY,
	FreeUsersId int FOREIGN KEY REFERENCES freeusers(FreeUsersId),
	PremiumUsersId int FOREIGN KEY REFERENCES premiumusers(PremiumUsersId),
	UserName varchar(100) NOT NULL,
	UserEmail varchar(100) NOT NULL,
	UserPassword varchar(100) NOT NULL,
	UserPhone char(12)
)

create table Admins(
	AdminId int identity(1,1) PRIMARY KEY,
	AdminName varchar(100) NOT NULL,
	AdminPhone char(12) NOT NULL
)

create table Promotion(
	PromotionId int identity(1,1) PRIMARY KEY,
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	AdminId int FOREIGN KEY REFERENCES admins(AdminId),
	PromotionName varchar(100) NOT NULL,
	CreatedAt datetimeoffset Default GetDate(),
	CreatedBy varchar(100) NOT NULL,
	UpdatedAt datetimeoffset Default GetDate(),
	UpdatedBy varchar(100) NOT NULL
)

create table News(
	NewsId int identity(1,1) PRIMARY KEY,
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	AdminId int FOREIGN KEY REFERENCES admins(AdminId),
	NewsName varchar(100) NOT NULL,
	CreatedAt datetimeoffset Default GetDate(),
	CreatedBy varchar(100) NOT NULL,
	UpdatedAt datetimeoffset Default GetDate(),
	UpdatedBy varchar(100) NOT NULL
)

create table Report(
	ReportId int identity(1,1) PRIMARY KEY,
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	AdminId int FOREIGN KEY REFERENCES admins(AdminId),
	ReportName varchar(100) NOT NULL,
	ReportDescription varchar(200) NOT NULL,
	CreatedAt datetimeoffset Default GetDate(),
	CreatedBy varchar(100) NOT NULL
)

create table Suspends(
	SuspendId int identity(1,1) PRIMARY KEY,
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	AdminId int FOREIGN KEY REFERENCES admins(AdminId),
	SuspendName varchar(100) NOT NULL,
	SuspendDescription varchar(200) NOT NULL
)

create table Forum(
	ForumId int identity(1,1) PRIMARY KEY,
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	ReportId int FOREIGN KEY REFERENCES Report(ReportId),
	ForumName varchar(100) NOT NULL,
	CreatedAt datetimeoffset Default GetDate(),
	CreatedBy varchar(100) NOT NULL,
	UpdatedAt datetimeoffset Default GetDate(),
	UpdatedBy varchar(100) NOT NULL
)

create table Ads(
	AdsId int identity(1,1) PRIMARY KEY,
	AdsName varchar(100) NOT NULL,
	AdsDescription varchar(200) NOT NULL
)

create table VideoCategory(
	VideoCategoryId int identity(1,1) PRIMARY KEY,
	VideoCategoryName varchar(100) NOT NULL
)

create table Video(
	VideoId int identity(1,1) PRIMARY KEY,
	VideoCategoryId int FOREIGN KEY REFERENCES VideoCategory(VideoCategoryId),
	UsersId int FOREIGN KEY REFERENCES Users(UsersId),
	AdsId int FOREIGN KEY REFERENCES Ads(AdsId),
	VideoName varchar(100) NOT NULL,
	VideoDescription varchar(200) NOT NULL
)




